# FX Calculator

This is a currency conversion app.

Following are the details for the app:

1. Runs for list of currencies:
AUD, CAD, CNY, CZK, DKK, EUR, GBP, JPY, NOK, NZD, USD
2. Support for any other currency can be added by adding them with valid data
3. Conversion data is as below: \
    AUDUSD: 0.8371 \
    CADUSD: 0.8711 \
    USDCNY: 6.1715 \
    EURUSD: 1.2315 \
    GBPUSD: 1.5683 \
    NZDUSD: 0.7750 \
    USDJPY: 119.95 \
    EURCZK: 27.6028 \
    EURDKK: 7.4405 \
    EURNOK: 8.6651
4. Decimal precision for the currencies as follows:\
    AUD: 2\
    CAD: 2\
    CNY: 2\
    CZK: 2\
    DKK: 2\
    EUR: 2\
    GBP: 2\
    JPY: 0\
    NOK: 2\
    NZD: 2\
    USD: 2 
5. App runs on the logic that conversion can happen via below logic:\
    1. 1:1 (Unity) the rate is always 1
    2. D (Direct feed) - eg. the currency pair can be looked up directly.
    3. Inv (Inverted) - eg. the currency pair can be looked up if base and terms are flipped (and rate =
       1/rate) 
    4. CCY in order to calculate this rate, you need to cross via on or more currencies.
## Available Scripts

In the project directory, you can run:

### `npm install`
Before you try to run this app run above command to resolve the dependencies

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `npm run test`

Launches the test runner in the interactive watch mode.\

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!


### `git clone`
git clone https://JasvinderAnand@bitbucket.org/JasvinderAnand/fx-calculator.git

Above command can be used on cli to clone this project