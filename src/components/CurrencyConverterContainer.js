import Accordion from 'react-bootstrap/Accordion';
import Card from 'react-bootstrap/Card';
import Button from 'react-bootstrap/Button';
import SingleSelect from '../common/SingleSelect';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import React, {useEffect, useState} from 'react';
import {AppAlerts} from '../common/AppAlerts';
import {useDispatch, useSelector} from 'react-redux';
import {currencyListSelector, fetchCurrencies} from '../store/slices/CurrencySlice';
import {errorListSelector, updateErrors} from '../store/slices/ErrorSlice';
import {currencyConversionSelector, fetchCurrencyConversions} from '../store/slices/ConversionSlice';
import _ from 'lodash';
import {
    convertBaseToTerms,
    validateIfConversionIsPossible,
} from '../utils/CurrencyUtils';
import {decimalPrecisionSelector, fetchDecimalPrecision} from '../store/slices/DecimalPrecisionSlice';
import {coversionRateSelector, fetchCoversionRates} from '../store/slices/ConversionRateSlice';
import {CONVERSION_ERROR} from '../constants/Constants';

const CurrencyConverterContainer = () => {
    const dispatch = useDispatch();
    const listOfCurrency = useSelector(currencyListSelector);
    const listOfErrors = useSelector(errorListSelector);
    const currencyConversionData = useSelector(currencyConversionSelector);
    const decimalPrecisionData = useSelector(decimalPrecisionSelector);
    const currencyConversionRate = useSelector(coversionRateSelector);

    const [baseCurrency, setBaseCurrency] = useState('');
    const [termsCurrency, setTermsCurrency] = useState('');
    const [baseCurrenyAmount, setBaseCurrencyAmount] = useState(0);
    const [convertedAmount, setConvertedtAmount] = useState(0);

    useEffect(() => {
        dispatch(fetchCurrencies());
        dispatch(fetchCurrencyConversions());
        dispatch(fetchDecimalPrecision());
        dispatch(fetchCoversionRates());
    }, []);

    const onChangeSelect = (e, type) => {
        if (type === 'termsCurrency') {
            setTermsCurrency(e.target.value);
            calculate(baseCurrency, e.target.value, baseCurrenyAmount);
        } else {
            setBaseCurrency(e.target.value);
            calculate(e.target.value, termsCurrency, baseCurrenyAmount);
        }
    };

    const onChangeInput = (e, type) => {
        if (type === 'baseAmount') {
            const currenncyAmount = e.target.valueAsNumber ? e.target.valueAsNumber : 0;
            setBaseCurrencyAmount(currenncyAmount);
            calculate(baseCurrency, termsCurrency, currenncyAmount);
        }
    };

    const calculate = (baseCurrency, termsCurrency, currenncyAmount) => {
        let errors = validateIfConversionIsPossible(baseCurrency, termsCurrency, currencyConversionData);
        if (!_.isEmpty(errors)) {
            dispatch(updateErrors(errors));
            setConvertedtAmount(0);
        } else {
            const result = convertBaseToTerms(baseCurrency, termsCurrency, currenncyAmount, currencyConversionData, decimalPrecisionData, currencyConversionRate);
            if (!result.includes('NaN')) {
                setConvertedtAmount(result);
                dispatch(updateErrors({}));
            } else {
                setConvertedtAmount(0);
                dispatch(updateErrors(CONVERSION_ERROR));
            }
        }
    };

    return (
        <>
            <Row className={'m-5'}>
                <Col>
                    <Accordion defaultActiveKey="0">
                        <Card>
                            <Card.Header>
                                <Accordion.Toggle as={Button} variant="link" eventKey="0">
                                    FX-Calculator
                                </Accordion.Toggle>
                            </Card.Header>
                            <Accordion.Collapse eventKey="0">
                                <Card.Body>
                                    <Container>
                                        <Row className={'mb-3'}>
                                            <Col>
                                                <SingleSelect listOfCurrency={listOfCurrency} label={'Base Currency'}
                                                              onChange={(e) => onChangeSelect(e, 'baseCurrency')}/>
                                            </Col>
                                            <Col>
                                                <label> Base Currency Amount </label>
                                                <input type="number" className="form-control" min="0"
                                                       data-testid={"base-input"}
                                                       onChange={(e) => onChangeInput(e, 'baseAmount')}
                                                       defaultValue={0}/>
                                            </Col>
                                            <Col>
                                                <SingleSelect listOfCurrency={listOfCurrency} label={'Terms Currency'}
                                                              onChange={(e) => onChangeSelect(e, 'termsCurrency')}/>
                                            </Col>

                                        </Row>
                                        {listOfErrors ?
                                            <AppAlerts listOfErrors={listOfErrors} alertType={'danger'}/> : null}
                                        {convertedAmount ?
                                            <AppAlerts convertedAmount={convertedAmount} baseCurrency={baseCurrency}
                                                       baseCurrenyAmount={baseCurrenyAmount}
                                                       alertType={'success'}/> : null}
                                    </Container>
                                </Card.Body>
                            </Accordion.Collapse>
                        </Card>
                    </Accordion>
                </Col>
            </Row>
        </>
    );
};

export default CurrencyConverterContainer;
