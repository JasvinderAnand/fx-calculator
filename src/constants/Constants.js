export const BASE_CURRENCY_VALIDATION_ERROR = {'Conversion Error': 'Unable to convert, conversion value not available. Try changing base currency.'};
export const TERMS_CURRENCY_VALIDATION_ERROR = {'Conversion Error': 'Unable to convert, conversion value not available. Try changing terms currency.'};
export const ONE_ON_ONE = '1:1';
export const DIRECT = 'D';
export const INVERTED = 'Inv';
export const CONVERSION_ERROR = {'Conversion Error': 'Unable to convert, conversion data not available.'};