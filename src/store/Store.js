import {combineReducers, applyMiddleware, compose, createStore} from 'redux';
import thunk from 'redux-thunk';
import {createLogger} from 'redux-logger';
import {currencyListReducer} from './slices/CurrencySlice';
import {errorListReducer} from './slices/ErrorSlice';
import {currencyConversionReducer} from './slices/ConversionSlice';
import {decimalPrecisionReducer} from './slices/DecimalPrecisionSlice';
import {coversionRateReducer} from './slices/ConversionRateSlice';

const logger = createLogger({level: 'debug'});
const middleware = [thunk, logger];
const composeEnhancer = typeof window === 'object' && window['__REDUX_DEVTOOLS_EXTENSION_COMPOSE__']
    ? window['__REDUX_DEVTOOLS_EXTENSION_COMPOSE__']({
        trace: true,
        traceLimit: 25
    }) : compose;

const store = createStore(
    combineReducers({
        currencyList: currencyListReducer,
        errorList: errorListReducer,
        currencyConversion: currencyConversionReducer,
        decimalPrecision: decimalPrecisionReducer,
        coversionRate: coversionRateReducer

    }), {},
    composeEnhancer(applyMiddleware(...middleware))
);

export default store;