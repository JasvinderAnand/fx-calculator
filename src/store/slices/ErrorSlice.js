import {createSlice} from '@reduxjs/toolkit';
import {fetchCurrencies} from './CurrencySlice';
import {fetchCurrencyConversions} from './ConversionSlice';
import {fetchDecimalPrecision} from './DecimalPrecisionSlice';
import {fetchCoversionRates} from './ConversionRateSlice';

const initialState = [];

const update = (state, mutations) =>
    Object.assign({}, state, mutations);

const errorListSlice = createSlice({
    name: 'errorListSlice',
    initialState,
    reducers: {
        updateErrors: (state, action) => {
            state = action.payload;
            return state;
        }
    },
    extraReducers: {
        [fetchCurrencies.rejected.toString()]: (state, action) => {
            state = update(state, {'Currency List Fetch Error': action.error.message});
            return state;
        },
        [fetchCurrencyConversions.rejected.toString()]: (state, action) => {
            state = update(state, {'Currency Conversion Fetch Error': action.error.message});
            return state;
        },
        [fetchDecimalPrecision.rejected.toString()]: (state, action) => {
            state = update(state, {'Decimal Precision Fetch Error': action.error.message});
            return state;
        },
        [fetchCoversionRates.rejected.toString()]: (state, action) => {
            state = update(state, {'Conversion Rate Fetch Error': action.error.message});
            return state;
        }
    }
});

export const {updateErrors} = errorListSlice.actions;
export const errorListSelector = (state) => state.errorList;
export const errorListReducer = errorListSlice.reducer;