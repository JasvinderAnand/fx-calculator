import {createAsyncThunk, createSlice} from '@reduxjs/toolkit';
import {getCurrencies} from '../../api/FxCalculatorApi';
import _ from 'lodash';

export const GET_CURRENCY_LIST_ACTION = 'currencyList/get';

export const fetchCurrencies = createAsyncThunk(
    GET_CURRENCY_LIST_ACTION,
    async () => {
        const response = await getCurrencies();
        /*Assuming empty response for error message*/
        if (_.isEmpty(response)) {
            throw new Error('error occurred while fetching currency list');
        }
        return response;
    }
);

const initialState = [];

const currencyListSlice = createSlice({
    name: 'currencyList',
    initialState,
    reducers: {},
    extraReducers: {
        [fetchCurrencies.fulfilled.toString()]: (state, action) => {
            state = action.payload;
            return state;
        }
    }
});

export const currencyListSelector = (state) => state.currencyList;
export const currencyListReducer = currencyListSlice.reducer;