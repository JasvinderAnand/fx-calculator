import {createAsyncThunk, createSlice} from '@reduxjs/toolkit';
import {getCurrencyConversions} from '../../api/FxCalculatorApi';
import _ from 'lodash';

export const GET_CURRENCY_CONVERSION_ACTION = 'currencyConversions/get';

export const fetchCurrencyConversions = createAsyncThunk(
    GET_CURRENCY_CONVERSION_ACTION,
    async () => {
        const response = await getCurrencyConversions();
        /*Assuming empty response for error message*/
        if (_.isEmpty(response)) {
            throw new Error('error while fetching conversion data');
        }
        return response;
    }
);

const initialState = [];

const currencyConversionSlice = createSlice({
    name: 'currencyConversion',
    initialState,
    reducers: {},
    extraReducers: {
        [fetchCurrencyConversions.fulfilled.toString()]: (state, action) => {
            state = action.payload;
            return state;
        }
    }
});

export const currencyConversionSelector = (state) => state.currencyConversion;
export const currencyConversionReducer = currencyConversionSlice.reducer;