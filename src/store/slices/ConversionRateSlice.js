import {createAsyncThunk, createSlice} from '@reduxjs/toolkit';
import {getCoversionRates} from '../../api/FxCalculatorApi';
import _ from 'lodash';

export const GET_CONVERSION_RATE_ACTION = 'conversionRate/get';

export const fetchCoversionRates = createAsyncThunk(
    GET_CONVERSION_RATE_ACTION,
    async () => {
        const response = await getCoversionRates();
        /*Assuming empty response for error message*/
        if (_.isEmpty(response)) {
            throw new Error('error while fetching conversion rate data');
        }
        return response;
    }
);

const initialState = [];

const coversionRateSlice = createSlice({
    name: 'coversionRate',
    initialState,
    reducers: {},
    extraReducers: {
        [fetchCoversionRates.fulfilled.toString()]: (state, action) => {
            state = action.payload;
            return state;
        }
    }
});

export const coversionRateSelector = (state) => state.coversionRate;
export const coversionRateReducer = coversionRateSlice.reducer;