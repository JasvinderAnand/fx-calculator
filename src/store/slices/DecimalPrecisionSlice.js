import {createAsyncThunk, createSlice} from '@reduxjs/toolkit';
import {getDecimalPrecision} from '../../api/FxCalculatorApi';
import _ from 'lodash';

export const GET_CURRENCY_CONVERSION_ACTION = 'decimalPrecision/get';

export const fetchDecimalPrecision = createAsyncThunk(
    GET_CURRENCY_CONVERSION_ACTION,
    async () => {
        const response = await getDecimalPrecision();
        /*Assuming empty response for error message*/
        if (_.isEmpty(response)) {
            throw new Error('error while fetching decimal precision data');
        }
        return response;
    }
);

const initialState = [];

const decimalPrecisionSlice = createSlice({
    name: 'decimalPrecision',
    initialState,
    reducers: {},
    extraReducers: {
        [fetchDecimalPrecision.fulfilled.toString()]: (state, action) => {
            state = action.payload;
            return state;
        }
    }
});

export const decimalPrecisionSelector = (state) => state.decimalPrecision;
export const decimalPrecisionReducer = decimalPrecisionSlice.reducer;