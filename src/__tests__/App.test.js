import {render, screen} from '@testing-library/react';
import App from '../App';
import store from '../store/Store';
import {Provider} from 'react-redux';
import renderer from 'react-test-renderer';

test('renders FX-Calculator', () => {
    render(
        <Provider store={store}>
            <App/>
        </Provider>
    );
    const linkElement = screen.getByText(/FX-Calculator/i);
    expect(linkElement).toBeInTheDocument();
});

test('check if labels are present', () => {
    render(
        <Provider store={store}>
            <App/>
        </Provider>
    );
    const label = screen.getByText('Base Currency');
    expect(label).toBeInTheDocument();
    const label1 = screen.getByText('Terms Currency');
    expect(label1).toBeInTheDocument();
    const label2 = screen.getByText('Base Currency Amount');
    expect(label2).toBeInTheDocument();
});

test('check if renders correctly and snapshot matches', () => {
    const app = renderer
        .create(<Provider store={store}>
            <App/>
        </Provider>).toJSON();
    expect(app).toMatchSnapshot('./__snapshots__/App.test.js.snap');
});
