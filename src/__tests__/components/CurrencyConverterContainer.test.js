import {render, cleanup} from '@testing-library/react';
import store from '../../store/Store';
import {Provider} from 'react-redux';
import CurrencyConverterContainer from '../../components/CurrencyConverterContainer';
import React from 'react';
import userEvent from "@testing-library/user-event";

afterEach(cleanup);

beforeAll(() => {

});

describe('Test Base Currency', () => {
    const {getByTestId} = render(
        <Provider store={store}>
            <CurrencyConverterContainer/>
        </Provider>);

    it('check default value for input', () => {
        const input = getByTestId('base-input');
        expect(input.value).toBe("0");
        userEvent.clear(input);
        userEvent.type(input, '10');
        expect(input.value).toBe('10');
    });

    it('check default value and test on change for base currency', () => {
        const {getByTestId} = render(
            <Provider store={store}>
                <CurrencyConverterContainer/>
            </Provider>);
        const input = getByTestId('Base Currency-select');
        expect(input.value).toBe('AED');
        userEvent.selectOptions(input, 'AUD');
        expect(input.value).toBe('AUD');
    });

    it('check default value and test on change for terms currency', () => {
        const {getByTestId} = render(
            <Provider store={store}>
                <CurrencyConverterContainer/>
            </Provider>);

        const input = getByTestId('Terms Currency-select');
        expect(input.value).toBe('AED');
        userEvent.selectOptions(input, 'AUD');
        expect(input.value).toBe('AUD');
    });

    it('test invalid base currency', () => {
        const {getByTestId} = render(
            <Provider store={store}>
                <CurrencyConverterContainer/>
            </Provider>);
        const input = getByTestId('Base Currency-select');
        expect(input.value).toBe('AED');

        const input1 = getByTestId('base-input');
        expect(input1.value).toBe("0");
        userEvent.clear(input1);
        userEvent.type(input1, '10');
        expect(input1.value).toBe('10');
        expect(getByTestId('error-alert')).toHaveTextContent('Conversion Error: Unable to convert, conversion value not available. Try changing base currency.');
    });

    it('test invalid terms currency', () => {
        const {getByTestId} = render(
            <Provider store={store}>
                <CurrencyConverterContainer/>
            </Provider>);
        const input = getByTestId('Base Currency-select');
        expect(input.value).toBe('AED');
        userEvent.selectOptions(input, 'AUD');
        expect(input.value).toBe('AUD');

        const input1 = getByTestId('base-input');
        expect(input1.value).toBe("0");
        userEvent.clear(input1);
        userEvent.type(input1, '10');
        expect(input1.value).toBe('10');
        expect(getByTestId('error-alert')).toHaveTextContent('Conversion Error: Unable to convert, conversion value not available. Try changing terms currency.');
    });

    it('test calculation error if path to calculate currency is not present', () => {
        const {getByTestId} = render(
            <Provider store={store}>
                <CurrencyConverterContainer/>
            </Provider>);
        const input = getByTestId('Base Currency-select');
        expect(input.value).toBe('AED');
        userEvent.selectOptions(input, 'CZK');
        expect(input.value).toBe('CZK');

        const input1 = getByTestId('base-input');
        expect(input1.value).toBe("0");
        userEvent.clear(input1);
        userEvent.type(input1, '10');
        expect(input1.value).toBe('10');

        const input2 = getByTestId('Terms Currency-select');
        expect(input2.value).toBe('AED');
        userEvent.selectOptions(input2, 'AUD');
        expect(input2.value).toBe('AUD');

        expect(getByTestId('error-alert')).toHaveTextContent('Conversion Error: Unable to convert, conversion data not available.');
    });

    it('test success 1-1(unity) mapping', () => {
        const {getByTestId} = render(
            <Provider store={store}>
                <CurrencyConverterContainer/>
            </Provider>);
        const input = getByTestId('Base Currency-select');
        expect(input.value).toBe('AED');
        userEvent.selectOptions(input, 'AUD');
        expect(input.value).toBe('AUD');

        const input1 = getByTestId('base-input');
        expect(input1.value).toBe("0");
        userEvent.clear(input1);
        userEvent.type(input1, '10');
        expect(input1.value).toBe('10');

        const input2 = getByTestId('Terms Currency-select');
        expect(input2.value).toBe('AED');
        userEvent.selectOptions(input2, 'AUD');
        expect(input2.value).toBe('AUD');

        expect(getByTestId('error-alert')).toHaveTextContent('Conversion Successful: 10 AUD equals A$10.00');
    });

    it('test success D(Direct) mapping', () => {
        const {getByTestId} = render(
            <Provider store={store}>
                <CurrencyConverterContainer/>
            </Provider>);
        const input = getByTestId('Base Currency-select');
        expect(input.value).toBe('AED');
        userEvent.selectOptions(input, 'AUD');
        expect(input.value).toBe('AUD');

        const input1 = getByTestId('base-input');
        expect(input1.value).toBe("0");
        userEvent.clear(input1);
        userEvent.type(input1, '10');
        expect(input1.value).toBe('10');

        const input2 = getByTestId('Terms Currency-select');
        expect(input2.value).toBe('AED');
        userEvent.selectOptions(input2, 'USD');
        expect(input2.value).toBe('USD');
        /* matching only $ as conversion rate may change in future */
        expect(getByTestId('error-alert')).toHaveTextContent('Conversion Successful: 10 AUD equals $');
    });

    it('test success Inv(inverted) mapping', () => {
        const {getByTestId} = render(
            <Provider store={store}>
                <CurrencyConverterContainer/>
            </Provider>);
        const input = getByTestId('Base Currency-select');
        expect(input.value).toBe('AED');
        userEvent.selectOptions(input, 'USD');
        expect(input.value).toBe('USD');

        const input1 = getByTestId('base-input');
        expect(input1.value).toBe("0");
        userEvent.clear(input1);
        userEvent.type(input1, '10');
        expect(input1.value).toBe('10');

        const input2 = getByTestId('Terms Currency-select');
        expect(input2.value).toBe('AED');
        userEvent.selectOptions(input2, 'AUD');
        expect(input2.value).toBe('AUD');
        /* matching only $ as conversion rate may change in future */
        expect(getByTestId('error-alert')).toHaveTextContent('Conversion Successful: 10 USD equals A$');
    });

    it('test success CCY(crossing one currency) mapping', () => {
        const {getByTestId} = render(
            <Provider store={store}>
                <CurrencyConverterContainer/>
            </Provider>);
        const input = getByTestId('Base Currency-select');
        expect(input.value).toBe('AED');
        userEvent.selectOptions(input, 'AUD');
        expect(input.value).toBe('AUD');

        const input1 = getByTestId('base-input');
        expect(input1.value).toBe("0");
        userEvent.clear(input1);
        userEvent.type(input1, '10');
        expect(input1.value).toBe('10');

        const input2 = getByTestId('Terms Currency-select');
        expect(input2.value).toBe('AED');
        userEvent.selectOptions(input2, 'CAD');
        expect(input2.value).toBe('CAD');
        /* matching only $ as conversion rate may change in future */
        expect(getByTestId('error-alert')).toHaveTextContent('Conversion Successful: 10 AUD equals CA$');
    });

    it('test success CCY(crossing two currency) mapping', () => {
        const {getByTestId} = render(
            <Provider store={store}>
                <CurrencyConverterContainer/>
            </Provider>);
        const input = getByTestId('Base Currency-select');
        expect(input.value).toBe('AED');
        userEvent.selectOptions(input, 'USD');
        expect(input.value).toBe('USD');

        const input1 = getByTestId('base-input');
        expect(input1.value).toBe("0");
        userEvent.clear(input1);
        userEvent.type(input1, '10');
        expect(input1.value).toBe('10');

        const input2 = getByTestId('Terms Currency-select');
        expect(input2.value).toBe('AED');
        userEvent.selectOptions(input2, 'CZK');
        expect(input2.value).toBe('CZK');
        /* matching only $ as conversion rate may change in future */
        expect(getByTestId('error-alert')).toHaveTextContent('Conversion Successful: 10 USD equals CZK');
    });

});