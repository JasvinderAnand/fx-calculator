import {
    convertBaseToTerms,
    validateBaseCurrency,
    validateIfConversionIsPossible,
    validateTermsCurrency
} from "../../utils/CurrencyUtils";


describe('test currency util methods', () => {
    const conversionData = {
        "conversions": [
            {
                "base": "AUD",
                "AUD": "1:1",
                "CAD": "USD",
                "CNY": "USD",
                "CZK": "USD",
                "DKK": "USD",
                "EUR": "USD",
                "GBP": "USD",
                "JPY": "USD",
                "NOK": "USD",
                "NZD": "USD",
                "USD": "D"
            },
            {
                "base": "CAD",
                "AUD": "USD",
                "CAD": "1:1",
                "CNY": "USD",
                "CZK": "USD",
                "DKK": "USD",
                "EUR": "USD",
                "GBP": "USD",
                "JPY": "USD",
                "NOK": "USD",
                "NZD": "USD",
                "USD": "D"
            },
            {
                "base": "CNY",
                "AUD": "USD",
                "CAD": "USD",
                "CNY": "1:1",
                "CZK": "USD",
                "DKK": "USD",
                "EUR": "USD",
                "GBP": "USD",
                "JPY": "USD",
                "NOK": "USD",
                "NZD": "USD",
                "USD": "D"
            },
            {
                "base": "CZK",
                "AUD": "USD",
                "CAD": "USD",
                "CNY": "USD",
                "CZK": "1:1",
                "DKK": "EUR",
                "EUR": "Inv",
                "GBP": "USD",
                "JPY": "USD",
                "NOK": "EUR",
                "NZD": "USD",
                "USD": "EUR"
            },
            {
                "base": "DKK",
                "AUD": "USD",
                "CAD": "USD",
                "CNY": "USD",
                "CZK": "EUR",
                "DKK": "1:1",
                "EUR": "Inv",
                "GBP": "USD",
                "JPY": "USD",
                "NOK": "EUR",
                "NZD": "USD",
                "USD": "EUR"
            },
            {
                "base": "EUR",
                "AUD": "USD",
                "CAD": "USD",
                "CNY": "USD",
                "CZK": "D",
                "DKK": "D",
                "EUR": "1:1",
                "GBP": "USD",
                "JPY": "USD",
                "NOK": "D",
                "NZD": "USD",
                "USD": "D"
            },
            {
                "base": "GBP",
                "AUD": "USD",
                "CAD": "USD",
                "CNY": "USD",
                "CZK": "USD",
                "DKK": "USD",
                "EUR": "USD",
                "GBP": "1:1",
                "JPY": "USD",
                "NOK": "USD",
                "NZD": "USD",
                "USD": "D"
            },
            {
                "base": "JPY",
                "AUD": "USD",
                "CAD": "USD",
                "CNY": "USD",
                "CZK": "USD",
                "DKK": "USD",
                "EUR": "USD",
                "GBP": "USD",
                "JPY": "1:1",
                "NOK": "USD",
                "NZD": "USD",
                "USD": "Inv"
            },
            {
                "base": "NOK",
                "AUD": "USD",
                "CAD": "USD",
                "CNY": "USD",
                "CZK": "EUR",
                "DKK": "EUR",
                "EUR": "Inv",
                "GBP": "USD",
                "JPY": "USD",
                "NOK": "1:1",
                "NZD": "USD",
                "USD": "EUR"
            },
            {
                "base": "NZD",
                "AUD": "USD",
                "CAD": "USD",
                "CNY": "USD",
                "CZK": "USD",
                "DKK": "USD",
                "EUR": "USD",
                "GBP": "USD",
                "JPY": "USD",
                "NOK": "USD",
                "NZD": "1:1",
                "USD": "D"
            },
            {
                "base": "USD",
                "AUD": "Inv",
                "CAD": "Inv",
                "CNY": "Inv",
                "CZK": "EUR",
                "DKK": "EUR",
                "EUR": "Inv",
                "GBP": "Inv",
                "JPY": "D",
                "NOK": "EUR",
                "NZD": "Inv",
                "USD": "1:1"
            }
        ]
    };
    const conversionRates = {
        'AUDUSD': 0.8371,
        'CADUSD': 0.8711,
        'USDCNY': 6.1715,
        'EURUSD': 1.2315,
        'GBPUSD': 1.5683,
        'NZDUSD': 0.7750,
        'USDJPY': 119.95,
        'EURCZK': 27.6028,
        'EURDKK': 7.4405,
        'EURNOK': 8.6651
    };

    const decimalPrecision = {
        'AUD': 2,
        'CAD': 2,
        'CNY': 2,
        'CZK': 2,
        'DKK': 2,
        'EUR': 2,
        'GBP': 2,
        'JPY': 0,
        'NOK': 2,
        'NZD': 2,
        'USD': 2
    };
    it('test convertBaseToTerms for AUD ', () => {
        const terms = 'AUD';
        const bases = [
            'AUD',
            'CAD',
            'CNY',
            'CZK',
            'DKK',
            'EUR',
            'GBP',
            'JPY',
            'NOK',
            'NZD',
            'USD'
        ];
        const amount = 10;
        const results = [
            'A$10.00', 'A$10.41',
            'A$1.94', 'A$NaN',
            'A$NaN', 'A$14.71',
            'A$18.73', 'A$0.10',
            'A$NaN', 'A$9.26',
            'A$11.95'
        ];
        let i = 0;
        bases.map(base => {
            const result = convertBaseToTerms(base, terms, amount, conversionData, decimalPrecision, conversionRates);
            expect(result).toBe(results[i]);
            i++;
        });
    });

    it('test convertBaseToTerms for CAD ', () => {
        const terms = 'CAD';
        const bases = [
            'AUD',
            'CAD',
            'CNY',
            'CZK',
            'DKK',
            'EUR',
            'GBP',
            'JPY',
            'NOK',
            'NZD',
            'USD'
        ];
        const amount = 10;
        const results = [
            'CA$9.61',  'CA$10.00',
            'CA$1.86',  'CA$NaN',
            'CA$NaN',   'CA$14.14',
            'CA$18.00', 'CA$0.10',
            'CA$NaN',   'CA$8.90',
            'CA$11.48'
        ];
        let i = 0;
        bases.map(base => {
            const result = convertBaseToTerms(base, terms, amount, conversionData, decimalPrecision, conversionRates);
            expect(result).toBe(results[i]);
            i++;
        });
    });

    it('test convertBaseToTerms for CNY ', () => {
        const terms = 'CNY';
        const bases = [
            'AUD',
            'CAD',
            'CNY',
            'CZK',
            'DKK',
            'EUR',
            'GBP',
            'JPY',
            'NOK',
            'NZD',
            'USD'
        ];
        const amount = 10;
        const results = [
            'CN¥51.66', 'CN¥53.76',
            'CN¥10.00', 'CN¥NaN',
            'CN¥NaN',   'CN¥76.00',
            'CN¥96.79', 'CN¥0.51',
            'CN¥NaN',   'CN¥47.83',
            'CN¥61.72'
        ];
        let i = 0;
        bases.map(base => {
            const result = convertBaseToTerms(base, terms, amount, conversionData, decimalPrecision, conversionRates);
            expect(result).toBe(results[i]);
            i++;
        });
    });

    it('test convertBaseToTerms for CZK ', () => {
        const terms = 'CZK';
        const bases = [
            'AUD',
            'CAD',
            'CNY',
            'CZK',
            'DKK',
            'EUR',
            'GBP',
            'JPY',
            'NOK',
            'NZD',
            'USD'
        ];
        const amount = 10;
        const results = [
            'CZK 187.63', 'CZK 195.25',
            'CZK 36.32',  'CZK 10.00',
            'CZK 37.10',  'CZK 276.03',
            'CZK 351.52', 'CZK 1.87',
            'CZK 31.86',  'CZK 173.71',
            'CZK 224.14'
        ];
        let i = 0;
        bases.map(base => {
            const result = convertBaseToTerms(base, terms, amount, conversionData, decimalPrecision, conversionRates);
            expect(result).toBe(results[i]);
            i++;
        });
    });

    it('test convertBaseToTerms for DKK ', () => {
        const terms = 'DKK';
        const bases = [
            'AUD',
            'CAD',
            'CNY',
            'CZK',
            'DKK',
            'EUR',
            'GBP',
            'JPY',
            'NOK',
            'NZD',
            'USD'
        ];
        const amount = 10;
        const results = [
            'DKK 50.58', 'DKK 52.63',
            'DKK 9.79',  'DKK 2.70',
            'DKK 10.00', 'DKK 74.41',
            'DKK 94.75', 'DKK 0.50',
            'DKK 8.59',  'DKK 46.82',
            'DKK 60.42'
        ];
        let i = 0;
        bases.map(base => {
            const result = convertBaseToTerms(base, terms, amount, conversionData, decimalPrecision, conversionRates);
            expect(result).toBe(results[i]);
            i++;
        });
    });

    it('test convertBaseToTerms for EUR ', () => {
        const terms = 'EUR';
        const bases = [
            'AUD',
            'CAD',
            'CNY',
            'CZK',
            'DKK',
            'EUR',
            'GBP',
            'JPY',
            'NOK',
            'NZD',
            'USD'
        ];
        const amount = 10;
        const results = [
            '€6.80',  '€7.07',
            '€1.32',  '€0.36',
            '€1.34',  '€10.00',
            '€12.73', '€0.07',
            '€1.15',  '€6.29',
            '€8.12'
        ];
        let i = 0;
        bases.map(base => {
            const result = convertBaseToTerms(base, terms, amount, conversionData, decimalPrecision, conversionRates);
            expect(result).toBe(results[i]);
            i++;
        });
    });

    it('test convertBaseToTerms for GBP ', () => {
        const terms = 'GBP';
        const bases = [
            'AUD',
            'CAD',
            'CNY',
            'CZK',
            'DKK',
            'EUR',
            'GBP',
            'JPY',
            'NOK',
            'NZD',
            'USD'
        ];
        const amount = 10;
        const results = [
            '£5.34',  '£5.55',
            '£1.03',  '£NaN',
            '£NaN',   '£7.85',
            '£10.00', '£0.05',
            '£NaN',   '£4.94',
            '£6.38'
        ];
        let i = 0;
        bases.map(base => {
            const result = convertBaseToTerms(base, terms, amount, conversionData, decimalPrecision, conversionRates);
            expect(result).toBe(results[i]);
            i++;
        });
    });

    it('test convertBaseToTerms for JPY ', () => {
        const terms = 'GBP';
        const bases = [
            'AUD',
            'CAD',
            'CNY',
            'CZK',
            'DKK',
            'EUR',
            'GBP',
            'JPY',
            'NOK',
            'NZD',
            'USD'
        ];
        const amount = 10;
        const results = [
            '£5.34',  '£5.55',
            '£1.03',  '£NaN',
            '£NaN',   '£7.85',
            '£10.00', '£0.05',
            '£NaN',   '£4.94',
            '£6.38'
        ];
        let i = 0;
        bases.map(base => {
            const result = convertBaseToTerms(base, terms, amount, conversionData, decimalPrecision, conversionRates);
            expect(result).toBe(results[i]);
            i++;
        });
    });

    it('test convertBaseToTerms for NOK ', () => {
        const terms = 'NOK';
        const bases = [
            'AUD',
            'CAD',
            'CNY',
            'CZK',
            'DKK',
            'EUR',
            'GBP',
            'JPY',
            'NOK',
            'NZD',
            'USD'
        ];
        const amount = 10;
        const results = [
            'NOK 58.90',  'NOK 61.29',
            'NOK 11.40',  'NOK 3.14',
            'NOK 11.65',  'NOK 86.65',
            'NOK 110.35', 'NOK 0.59',
            'NOK 10.00',  'NOK 54.53',
            'NOK 70.36'
        ];
        let i = 0;
        bases.map(base => {
            const result = convertBaseToTerms(base, terms, amount, conversionData, decimalPrecision, conversionRates);
            expect(result).toBe(results[i]);
            i++;
        });
    });

    it('test convertBaseToTerms for NZD ', () => {
        const terms = 'NZD';
        const bases = [
            'AUD',
            'CAD',
            'CNY',
            'CZK',
            'DKK',
            'EUR',
            'GBP',
            'JPY',
            'NOK',
            'NZD',
            'USD'
        ];
        const amount = 10;
        const results = [
            'NZ$10.80', 'NZ$11.24',
            'NZ$2.09',  'NZ$NaN',
            'NZ$NaN',   'NZ$15.89',
            'NZ$20.24', 'NZ$0.11',
            'NZ$NaN',   'NZ$10.00',
            'NZ$12.90'
        ];
        let i = 0;
        bases.map(base => {
            const result = convertBaseToTerms(base, terms, amount, conversionData, decimalPrecision, conversionRates);
            expect(result).toBe(results[i]);
            i++;
        });
    });

    it('test convertBaseToTerms for USD ', () => {
        const terms = 'USD';
        const bases = [
            'AUD',
            'CAD',
            'CNY',
            'CZK',
            'DKK',
            'EUR',
            'GBP',
            'JPY',
            'NOK',
            'NZD',
            'USD'
        ];
        const amount = 10;
        const results = [
            '$8.37',  '$8.71',
            '$1.62',  '$0.45',
            '$1.66',  '$12.32',
            '$15.68', '$0.08',
            '$1.42',  '$7.75',
            '$10.00'
        ];
        let i = 0;
        bases.map(base => {
            const result = convertBaseToTerms(base, terms, amount, conversionData, decimalPrecision, conversionRates);
            expect(result).toBe(results[i]);
            i++;
        });
    });

    it('test validateBaseCurrency for USD ', () => {
        const base = 'USD';
        const result = validateBaseCurrency(base, conversionData);
        expect(result).toBe(true);
    });

    it('test validateBaseCurrency for AED ', () => {
        const base = 'AED';
        const result = validateBaseCurrency(base, conversionData);
        expect(result).toBe(false);
    });

    it('test validateTermsCurrency for AED ', () => {
        const terms = 'AED';
        const result = validateTermsCurrency(terms, conversionData);
        expect(result).toBe(false);
    });

    it('test validateTermsCurrency for AUD ', () => {
        const terms = 'AUD';
        const result = validateTermsCurrency(terms, conversionData);
        expect(result).toBe(true);
    });

    it('test validateIfConversionIsPossible for AUD and USD', () => {
        const base = 'AUD';
        const terms = 'USD';
        const result = validateIfConversionIsPossible(base, terms,conversionData);
        expect(result).toBe(null);
    });

    it('test validateIfConversionIsPossible for AED and USD', () => {
        const base = 'AED';
        const terms = 'USD';
        const result = validateIfConversionIsPossible(base, terms,conversionData);
        expect(result).toStrictEqual({'Conversion Error': 'Unable to convert, conversion value not available. Try changing base currency.'});
    });

    it('test validateIfConversionIsPossible for AUD and AED', () => {
        const base = 'AUD';
        const terms = 'AED';
        const result = validateIfConversionIsPossible(base, terms,conversionData);
        expect(result).toStrictEqual({'Conversion Error': 'Unable to convert, conversion value not available. Try changing terms currency.'});
    });
});