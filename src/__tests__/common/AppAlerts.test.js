import {render} from '@testing-library/react';
import store from '../../store/Store';
import {Provider} from 'react-redux';
import {AppAlerts} from "../../common/AppAlerts";
import React from "react";

describe('test App Alerts component', () => {

    it('check success message is displayed', () => {
        const props = {
            alertType: "success",
            baseCurrency: "USD",
            baseCurrenyAmount: 1,
            convertedAmount: "A$1.19"
        };

        const  {getByTestId} = render(
            <Provider store={store}>
                <AppAlerts {...props} />
            </Provider>
        );
        expect(getByTestId('error-alert')).toHaveTextContent('Conversion Successful: 1 USD equals A$1.19');
    });

    it('check error message is displayed ', () => {
        const props = {
            alertType: "danger",
            listOfErrors: {
                "Conversion Error": "Unable to convert, conversion value not available"
            }
        };

        const  {getByTestId} = render(
            <Provider store={store}>
                <AppAlerts {...props} />
            </Provider>
        );
        expect(getByTestId('error-alert')).toHaveTextContent("Conversion Error: Unable to convert, conversion value not available");
    });

});