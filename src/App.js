import './App.css';
import CurrencyConverterContainer from './components/CurrencyConverterContainer';
import { BrowserRouter as Router, Route } from 'react-router-dom';

function App() {
  return (
    <div className="App">
        <Router>
            <Route path="/" exact={true} component={CurrencyConverterContainer}/>
        </Router>
    </div>
  );
}

export default App;
