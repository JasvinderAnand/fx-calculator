import React from 'react';
import Form from 'react-bootstrap/Form';

const SingleSelect = (props) => {
    let options = [];
    options = props.listOfCurrency && props.listOfCurrency.length > 0 && props.listOfCurrency.map(currency => <option
        key={currency} value={currency}>{currency}</option>);
    return (
        <>
            <label>{props.label}</label>
            <Form.Control
                as="select"
                custom
                onChange={props.onChange}
                data-testid={props.label + "-select"}
            >
                {options}
            </Form.Control>
        </>
    );
};

export default SingleSelect;

