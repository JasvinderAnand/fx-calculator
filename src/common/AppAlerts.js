import React from 'react';
import Alert from 'react-bootstrap/Alert'

export const AppAlerts = (props) => {
    const notifications = [];
    if (props.listOfErrors) {
        for (const [key, value] of Object.entries(props.listOfErrors)) {
            notifications.push(key + ': ' + value);
        }
    } else if (props.convertedAmount) {
        notifications.push('Conversion Successful: ' + props.baseCurrenyAmount + ' ' + props.baseCurrency + ' equals ' + props.convertedAmount);
    }

    return (
        <>
            {notifications.map(error => <Alert  data-testid={"error-alert"} key={error + 1} variant={props.alertType}>{error}</Alert>)}
        </>
    );
};


