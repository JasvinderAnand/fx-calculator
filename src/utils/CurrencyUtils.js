import _ from 'lodash';
import {
    BASE_CURRENCY_VALIDATION_ERROR,
    DIRECT, INVERTED,
    ONE_ON_ONE,
    TERMS_CURRENCY_VALIDATION_ERROR
} from '../constants/Constants';

export const validateBaseCurrency = (base, conversionData) => {
    return conversionData && conversionData.conversions && conversionData.conversions.filter(data => data.base === base).length > 0;
};

export const validateTermsCurrency = (terms, conversionData) => {
    return conversionData.conversions.map(data => Object.keys(data))[0].filter(currency => currency === terms).length > 0;
};

export const validateIfConversionIsPossible = (baseCurrency, termsCurrency, currencyConversionData) => {
    if (!validateBaseCurrency(baseCurrency, currencyConversionData)) {
        return BASE_CURRENCY_VALIDATION_ERROR;
    }
    if (!validateTermsCurrency(termsCurrency, currencyConversionData)) {
        return TERMS_CURRENCY_VALIDATION_ERROR;
    }
    return null;
};

export const convertBaseToTerms = (base, terms, amount, conversionData, decimalPrecisionData, currencyConversionRate) => {
    const formatter = new Intl.NumberFormat('en',
        {
            style: 'currency',
            currency: terms,
            minimumFractionDigits: decimalPrecisionData[terms],
            currencyDisplay: 'symbol'
        }
    );
    const currencyMappedValue = conversionData.conversions.filter(data => data.base === base)[0][terms];
    //logic for 1:1 mapping
    if (base === terms || currencyMappedValue === ONE_ON_ONE) {
        return formatter.format(amount);
    } else if (currencyMappedValue === DIRECT) {
        //logic for Direct mapping
        if (currencyConversionRate[base + terms]) {
            return formatter.format(amount * currencyConversionRate[base + terms]);
        }
        return formatter.format(amount / currencyConversionRate[terms + base]);
    } else if (currencyMappedValue === INVERTED) {
        //logic for Inv Mapping
        if (currencyConversionRate[base + terms]) {
            return formatter.format(amount * currencyConversionRate[base + terms]);
        }
        return formatter.format(amount / currencyConversionRate[terms + base]);
    } else if (_.includes(Object.keys(conversionData.conversions.filter(data => data.base === base)[0]), currencyMappedValue)) {
        //logic for a valid CCY
        if (conversionData.conversions.filter(data => data.base === currencyMappedValue)[0][terms] === 'Inv') {
            // condition to check if intermediate mapping is Inv
            if (currencyConversionRate[currencyMappedValue + terms] && currencyConversionRate[base + currencyMappedValue]) {
                return formatter.format(amount * currencyConversionRate[base + currencyMappedValue] * currencyConversionRate[currencyMappedValue + terms]);
            } else if (currencyConversionRate[base + currencyMappedValue]) {
                return formatter.format(amount * currencyConversionRate[base + currencyMappedValue] / currencyConversionRate[terms + currencyMappedValue]);
            } else if (currencyConversionRate[currencyMappedValue + terms]) {
                return formatter.format(amount / currencyConversionRate[currencyMappedValue + base] * currencyConversionRate[currencyMappedValue + terms]);
            }
            return formatter.format(amount * 1 / currencyConversionRate[currencyMappedValue + base] / currencyConversionRate[terms + currencyMappedValue]);
        } else if (_.includes(Object.keys(conversionData.conversions.filter(data => data.base === base)[0]), conversionData.conversions.filter(data => data.base === currencyMappedValue)[0][terms])) {
            // condition to check if intermediate mapping is another valid CCY
            if (currencyConversionRate[base + currencyMappedValue] && currencyConversionRate[currencyMappedValue + conversionData.conversions.filter(data => data.base === currencyMappedValue)[0][terms]] && currencyConversionRate[conversionData.conversions.filter(data => data.base === currencyMappedValue)[0][terms] + terms]) {
                return formatter.format(amount * currencyConversionRate[base + currencyMappedValue] * currencyConversionRate[currencyMappedValue + conversionData.conversions.filter(data => data.base === currencyMappedValue)[0][terms]] * currencyConversionRate[conversionData.conversions.filter(data => data.base === currencyMappedValue)[0][terms] + terms]);
            } else if (currencyConversionRate[base + currencyMappedValue] && currencyConversionRate[currencyMappedValue + conversionData.conversions.filter(data => data.base === currencyMappedValue)[0][terms]]) {
                return formatter.format(amount * currencyConversionRate[base + currencyMappedValue] * currencyConversionRate[currencyMappedValue + conversionData.conversions.filter(data => data.base === currencyMappedValue)[0][terms]] / currencyConversionRate[terms + conversionData.conversions.filter(data => data.base === currencyMappedValue)[0][terms]]);
            } else if (currencyConversionRate[base + currencyMappedValue] && currencyConversionRate[conversionData.conversions.filter(data => data.base === currencyMappedValue)[0][terms] + terms]) {
                return formatter.format(amount * currencyConversionRate[base + currencyMappedValue] / currencyConversionRate[conversionData.conversions.filter(data => data.base === currencyMappedValue)[0][terms] + currencyMappedValue] * currencyConversionRate[conversionData.conversions.filter(data => data.base === currencyMappedValue)[0][terms] + terms]);
            } else if (currencyConversionRate[conversionData.conversions.filter(data => data.base === currencyMappedValue)[0][terms] + terms] && currencyConversionRate[currencyMappedValue + conversionData.conversions.filter(data => data.base === currencyMappedValue)[0][terms]]) {
                return formatter.format(amount / currencyConversionRate[currencyMappedValue + base] * currencyConversionRate[conversionData.conversions.filter(data => data.base === currencyMappedValue)[0][terms] + currencyMappedValue] * currencyConversionRate[conversionData.conversions.filter(data => data.base === currencyMappedValue)[0][terms] + terms]);
            } else if (currencyConversionRate[conversionData.conversions.filter(data => data.base === currencyMappedValue)[0][terms] + terms]) {
                return formatter.format(amount / currencyConversionRate[currencyMappedValue + base] / currencyConversionRate[conversionData.conversions.filter(data => data.base === currencyMappedValue)[0][terms] + currencyMappedValue] * currencyConversionRate[conversionData.conversions.filter(data => data.base === currencyMappedValue)[0][terms] + terms]);
            } else if (currencyConversionRate[currencyMappedValue + conversionData.conversions.filter(data => data.base === currencyMappedValue)[0][terms]]) {
                return formatter.format(amount / currencyConversionRate[currencyMappedValue + base] * currencyConversionRate[conversionData.conversions.filter(data => data.base === currencyMappedValue)[0][terms] + currencyMappedValue] / currencyConversionRate[terms + conversionData.conversions.filter(data => data.base === currencyMappedValue)[0][terms]]);
            }
        }

        if (currencyConversionRate[base + currencyMappedValue] && currencyConversionRate[currencyMappedValue + terms]) {
            return formatter.format(amount * currencyConversionRate[base + currencyMappedValue] * currencyConversionRate[currencyMappedValue + terms]);
        } else if (currencyConversionRate[currencyMappedValue + terms]) {
            return formatter.format(amount / currencyConversionRate[currencyMappedValue + base] * currencyConversionRate[currencyMappedValue + terms]);
        } else if (currencyConversionRate[base + currencyMappedValue]) {
            return formatter.format(amount * currencyConversionRate[base + currencyMappedValue] / currencyConversionRate[terms + currencyMappedValue]);
        }
    } else {
        return null;
    }


};